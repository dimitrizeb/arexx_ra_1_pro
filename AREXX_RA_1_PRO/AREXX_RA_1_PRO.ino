/*
 Name:    AREXΧ_RA_1_PRO.ino
 Created: 11/24/2019 8:33:57 PM
 Author:  Dimitris Zeibekakis
*/

#include <ESP8266WiFi.h>
#include <FS.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <DNSServer.h>

#pragma region Motor IDs
const String CLAW_ID = "19";
const String CLAW_ROTATE_ID = "20";
const String WRIST_ID = "23";
const String ELBOW_ID = "18";
const String SHOULDER_ID = "21";
const String BASE_ID = "22";
#pragma endregion

#pragma region Default Positions
const int CLAW_SERVO_DEFAULT_POS = 1080; //660-1500 range
const int SERVO_DEFAULT_POS = 1500; //500-2500 range
const String INITIALIZE_TIME_MILLIS = "200";
#pragma endregion

#pragma region Server Settings
IPAddress local_ip(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
const char* SSID = "AREXX_RA_1_PRO";
const char* PASSWORD = "";
const byte DNS_PORT = 53;
const byte SERVER_PORT = 80;
#pragma endregion

//Server Objects
AsyncWebServer server(SERVER_PORT);
DNSServer dnsServer;

//Arm Position
struct ArmPosition {
  int claw = CLAW_SERVO_DEFAULT_POS;
  int claw_rotate = SERVO_DEFAULT_POS;
  int wrist = SERVO_DEFAULT_POS;
  int elbow = SERVO_DEFAULT_POS;
  int shoulder = SERVO_DEFAULT_POS;
  int base = SERVO_DEFAULT_POS;
} current_arm_position;

struct PositionOffset {
  int claw = 0;
  int claw_rotate = 0;
  int wrist = 0;
  int elbow = 0;
  int shoulder = 0;
  int base = 0;
} position_offset;

void setup() {
  //Serial Communication 
  Serial.begin(115200);
  //Wait and print a new line to rid of the junk
  delay(10);
  Serial.println("");

  //Initialize servos to default position, otherwise they won't lock
  send_serial_command(CLAW_ID, CLAW_SERVO_DEFAULT_POS, INITIALIZE_TIME_MILLIS);
  send_serial_command(CLAW_ROTATE_ID, SERVO_DEFAULT_POS, INITIALIZE_TIME_MILLIS);
  send_serial_command(WRIST_ID, SERVO_DEFAULT_POS, INITIALIZE_TIME_MILLIS);
  send_serial_command(ELBOW_ID, SERVO_DEFAULT_POS, INITIALIZE_TIME_MILLIS);
  send_serial_command(SHOULDER_ID, SERVO_DEFAULT_POS, INITIALIZE_TIME_MILLIS);
  send_serial_command(BASE_ID, SERVO_DEFAULT_POS, INITIALIZE_TIME_MILLIS);

  //Start SPIFFS file system
  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  //Start the access point
  WiFi.softAPConfig(local_ip, gateway, subnet);
  WiFi.softAP(SSID, PASSWORD);
  IPAddress IP = WiFi.softAPIP();
  //Setup the DNS server redirecting all the domains to the AP IP
  dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
  dnsServer.start(DNS_PORT, "*", IP);
  //Configure for only 1 client
  change_max_clients(1);

  //Serve files and images to the html page
  server.on("/ra1-pro.jpg", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/ra1-pro.jpg", "image/jpg");
    });
  server.on("/arexx-logo.jpg", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/arexx-logo.jpg", "image/jpg");
    });
  server.on("/switch-off.svg", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/switch-off.svg", "image/svg+xml");
    });
  server.on("/switch-on.svg", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/switch-on.svg", "image/svg+xml");
    });
  server.on("/selectbox-arrow.png", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/selectbox-arrow.svg", "image/svg+xml");
    });
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/style.css", "text/css");
    });

  //Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/index.html");
    });

  //Route for Not Found
  server.onNotFound([](AsyncWebServerRequest* request) {
    request->send(SPIFFS, "/index.html");
    });

  //Handle Form Submission
  server.on("/get", HTTP_GET, [](AsyncWebServerRequest* request) {
    //Get the command and move the arm
    String command = request->getParam("command")->value();
    if (command.equals("run")) {
      move_arm_to_position(request);
    }
    else if (command.equals("reset")) {
      move_arm_to_position(request);
    }
    else if (command.equals("calibration")) {
      calibrate(request);
    }
    request->send(SPIFFS, "/index.html");
    });

  //Start server
  server.begin();
}

void loop() {
  //Must be called for the dns
  dnsServer.processNextRequest();
}

//Changes client number of the access point
void change_max_clients(byte client_num) {
  struct softap_config conf;
  wifi_softap_get_config(&conf);
  conf.max_connection = client_num;
  ETS_UART_INTR_DISABLE();
  wifi_softap_set_config(&conf);
  ETS_UART_INTR_ENABLE();
}

//Move arm to the position given
void move_arm_to_position(AsyncWebServerRequest* request) {
  //Get time
  String time_millis= request->getParam("time")->value();

  //Get claw value
  int claw_value = request->getParam("claw")->value().toInt();
  //If value changed, change value and move motor
  if (claw_value != current_arm_position.claw) {
    current_arm_position.claw = claw_value;
    send_serial_command(CLAW_ID, claw_value + position_offset.claw, time_millis);
  }

  //Get claw rotate value
  int claw_rotate_value = request->getParam("claw_rotate")->value().toInt();
  //If value changed, change value and move motor
  if (claw_rotate_value != current_arm_position.claw_rotate) {
    current_arm_position.claw_rotate = claw_rotate_value;
    send_serial_command(CLAW_ROTATE_ID, claw_rotate_value + position_offset.claw_rotate, time_millis);
  }

  //Get wrist value
  int wrist_value = request->getParam("wrist")->value().toInt();
  //If value changed, change value and move motor
  if (wrist_value != current_arm_position.wrist) {
    current_arm_position.wrist = wrist_value;
    send_serial_command(WRIST_ID, wrist_value + position_offset.wrist, time_millis);
  }

  //Get elbow value
  int elbow_value = request->getParam("elbow")->value().toInt();
  //If value changed, change value and move motor
  if (elbow_value != current_arm_position.elbow) {
    current_arm_position.elbow = elbow_value;
    //Elbow motor is reverse, so reverse the value sent
    elbow_value -= SERVO_DEFAULT_POS;
    elbow_value = SERVO_DEFAULT_POS - elbow_value;
    send_serial_command(ELBOW_ID, elbow_value + position_offset.elbow, time_millis);
  }

  //Get shoulder value
  int shoulder_value = request->getParam("shoulder")->value().toInt();
  //If value changed, change value and move motor
  if (shoulder_value != current_arm_position.shoulder) {
    current_arm_position.shoulder = shoulder_value;
    send_serial_command(SHOULDER_ID, shoulder_value + position_offset.shoulder, time_millis);
  }

  //Get base value
  int base_value = request->getParam("base")->value().toInt();
  //If value changed, change value and move motor
  if (base_value != current_arm_position.base) {
    current_arm_position.base = base_value;
    send_serial_command(BASE_ID, base_value + position_offset.base, time_millis);
  }
}


void calibrate(AsyncWebServerRequest* request) {
  //Get value
  int claw_value = request->getParam("claw")->value().toInt();
  //Calculate offset
  position_offset.claw = claw_value - CLAW_SERVO_DEFAULT_POS;

  //Get value
  int claw_rotate_value = request->getParam("claw_rotate")->value().toInt();
  //Calculate offset
  position_offset.claw_rotate = claw_rotate_value - SERVO_DEFAULT_POS;

  //Get value
  int wrist_value = request->getParam("wrist")->value().toInt();
  //Calculate offset
  position_offset.wrist = wrist_value - SERVO_DEFAULT_POS;

  //Get value
  int elbow_value = request->getParam("elbow")->value().toInt();
  //Calculate offset
  position_offset.elbow = elbow_value - SERVO_DEFAULT_POS;

  //Get value
  int shoulder_value = request->getParam("shoulder")->value().toInt();
  //Calculate offset
  position_offset.shoulder = shoulder_value - SERVO_DEFAULT_POS;

  //Get value
  int base_value = request->getParam("base")->value().toInt();
  //Calculate offset
  position_offset.base = base_value - SERVO_DEFAULT_POS;
}

//Formats the command and sends it via serial
void send_serial_command(String motor_id, int position, String time) {
  Serial.print("#");
  Serial.print(' ');
  Serial.print(motor_id);
  Serial.print(' ');
  Serial.print("P");
  Serial.print(String(position));
  Serial.print(' ');
  Serial.print("T");
  Serial.println(time);
}
