/* 
 * ****************************************************************************
 * Robotarm v3 - ROBOT BASE EXAMPLES
 * ****************************************************************************
 * Example: Robotarm keyboard
 * Author(s): 
 * ****************************************************************************
 * Description:
 * Basic I2C Slave example for RobotArm. 
 * Can be used for remote control via WIFI Expansion Board. 
 * 
 * ############################################################################
 * ATTENTION: Make sure you have calibrated the Robotarm, otherwise you
 * can damage the servos!
 * You can calibrate the servos with the selftest program. (press c)
 * ############################################################################
 * ****************************************************************************
 */
 
/*****************************************************************************/
// Includes:

#include "RobotArmBaseLib.h"// The Robotarm Robot Library.
							// Always needs to be included!

#include "RP6I2CSlaveTWI.h"

// The Slave Address on the I2C Bus can be specified here:
#define I2C_SLAVE_ADR 16


int overcurrent_shutdown = 0;
int current_check_enable = 1;

/*****************************************************************************/
// I2C Registers that can be read by the Master. 
// You can insert your own code here for data exchange with the Master.

#define I2C_REG_STATUS1 		 0
#define I2C_REG_STATUS2 		 1
#define I2C_REG_CURRENT1		 2
#define I2C_REG_CURRENT2		 3
#define I2C_REG_CURRENT3		 4
#define I2C_REG_CURRENT4		 5
#define I2C_REG_CURRENT5		 6
#define I2C_REG_CURRENT6		 7

/**
 * This function updates registers that the Master can read.
 * It is called frequently out of the Main loop. 
 */
void task_updateRegisters(void)
{
	if(!I2CTWI_readBusy) 
	{
		//I2CTWI_readRegisters[I2C_REG_STATUS1] = 0;
		//I2CTWI_readRegisters[I2C_REG_STATUS2] = 0;
	/*	I2CTWI_readRegisters[I2C_REG_CURRENT1] = Current_1; //readADC(ADC_CURRENT_1);
		I2CTWI_readRegisters[I2C_REG_CURRENT2] = Current_2; //readADC(ADC_CURRENT_2);
		I2CTWI_readRegisters[I2C_REG_CURRENT3] = Current_3; //readADC(ADC_CURRENT_3);
		I2CTWI_readRegisters[I2C_REG_CURRENT4] = Current_4; //readADC(ADC_CURRENT_4);
		I2CTWI_readRegisters[I2C_REG_CURRENT5] = Current_5; //readADC(ADC_CURRENT_5);
		I2CTWI_readRegisters[I2C_REG_CURRENT6] = Current_6; //readADC(ADC_CURRENT_6);
		*/
		
		Current_1 = readADC(ADC_CURRENT_1);
		Current_2 = readADC(ADC_CURRENT_2);
		Current_3 = readADC(ADC_CURRENT_3);
		Current_4 = readADC(ADC_CURRENT_4);
		Current_5 = readADC(ADC_CURRENT_5);
		Current_6 = readADC(ADC_CURRENT_6);
		
		I2CTWI_readRegisters[I2C_REG_CURRENT1] = Current_1;
		I2CTWI_readRegisters[I2C_REG_CURRENT2] = Current_2;
		I2CTWI_readRegisters[I2C_REG_CURRENT3] = Current_3;
		I2CTWI_readRegisters[I2C_REG_CURRENT4] = Current_4;
		I2CTWI_readRegisters[I2C_REG_CURRENT5] = Current_5;
		I2CTWI_readRegisters[I2C_REG_CURRENT6] = Current_6;

		if(current_check_enable)
		{
			if(Current_1 > max_current_servo1_v3){ overcurrent_shutdown = 1; } 
			if(Current_2 > max_current_servo2_v3){ overcurrent_shutdown = 2; } 
			if(Current_3 > max_current_servo3_v3){ overcurrent_shutdown = 3; } 
			if(Current_4 > max_current_servo4_v3){ overcurrent_shutdown = 4; }
			if(Current_5 > max_current_servo5_v3){ overcurrent_shutdown = 5; }
			if(Current_6 > max_current_servo6_v3){ overcurrent_shutdown = 6; }
		}
	}
}


/*****************************************************************************/
// Command Registers - these can be written by the Master.
// The other registers (read registers) can NOT be written to. The only way to
// communicate with the Robot is via specific commands. 
// Of course you can also add more registers if you like...

// ----------------------

#define I2C_REGW_CMD 0
#define I2C_REGW_CMD_PARAM1 1
#define I2C_REGW_CMD_PARAM2 2
#define I2C_REGW_CMD_PARAM3 3
#define I2C_REGW_CMD_PARAM4 4
#define I2C_REGW_CMD_PARAM5 5
#define I2C_REGW_CMD_PARAM6 6

// ----------------------

uint8_t cmd;
uint8_t param1;
uint8_t param2;
uint8_t param3;
uint8_t param4;
uint8_t param5;
uint8_t param6;

/**
 * Checks if a new Command has been received and also reads all 
 * paramters associated with this command.
 * It returns true if a new command has been received.
 */
uint8_t getCommand(void)
{
	if(I2CTWI_writeRegisters[I2C_REGW_CMD] && !I2CTWI_writeBusy) 
	{
		writeString_P("\n CMD!!! \n"); 
		cmd = I2CTWI_writeRegisters[I2C_REGW_CMD]; // store command register
		I2CTWI_writeRegisters[I2C_REGW_CMD] = 0; // clear command register (!!!)
		param1 = I2CTWI_writeRegisters[I2C_REGW_CMD_PARAM1]; // parameters 1-6...
		param2 = I2CTWI_writeRegisters[I2C_REGW_CMD_PARAM2];
		param3 = I2CTWI_writeRegisters[I2C_REGW_CMD_PARAM3];
		param4 = I2CTWI_writeRegisters[I2C_REGW_CMD_PARAM4];
		param5 = I2CTWI_writeRegisters[I2C_REGW_CMD_PARAM5];
		param6 = I2CTWI_writeRegisters[I2C_REGW_CMD_PARAM6];
		return true;
	}
	return false;
}


/*****************************************************************************/
// Command processor:


uint8_t leds = 1;

// Commands:
#define CMD_POWER_OFF 			1
#define CMD_POWER_ON 			2
#define CMD_SERVO_START			3
#define CMD_SERVO_MOVE			4
#define CMD_SERVO_MOVE_SPEED	5
#define CMD_BEEP				6
#define CMD_SET_LEDS			7

/**
 * This function checks if commands have been received and processes them.
 */ 
void task_commandProcessor(void)
{
	int p, s;

	if(getCommand()) 
	{
		setLED3(1);
		switch(cmd) 
		{
			case CMD_POWER_OFF:	
//				writeString_P("SERVO POWER OFF\n"); 
				Power_Off_Servos(); 
			break;
			case CMD_POWER_ON: 
//				writeString_P("SERVO POWER ON\n");
				current_check_enable = 0;
				Servo_Power_And_Start(); 
				setStopwatch2(0);
				startStopwatch2();
			break;
			case CMD_SERVO_START:
//				writeString_P("SERVO START POSITION\n"); 
				Start_position();
			break;
			case CMD_SERVO_MOVE:
//				writeString_P("MOVE SERVO\n"); 
				p = ((((uint16_t)param2)<<8)+param3);
				if(p > 1400) p = 1400;
				p -= 700;
				Move(param1, p);
			break;
			case CMD_SERVO_MOVE_SPEED:  // Range 0 - 1400!
//				writeString_P("MOVE SERVO AT SPEED\n"); 
				p = ((((uint16_t)param2)<<8)+param3);
				if(p > 1400) p = 1400;
				p -= 700;
				s = ((param4<<8)+param5);
				if (s > 1000) s = 1000;
				s_Move(param1, p, s);
			break;
			case CMD_BEEP:
//				writeString_P("BEEP\n"); 
				changeBeepsound(((param1<<8)+param2));
				setBeepsound();
				mSleep(((param3<<8)+param4));
				clearBeepsound();
			break;
			case CMD_SET_LEDS:
//				writeString_P("SET LEDS\n"); 
				setLEDs(param1);
			break;
			default:	
				writeString_P("\nERROR: UNKOWN COMMAND!\n\n"); 
				Power_Off_Servos(); 
			break;
		}
		mSleep(10);
		setLED3(0);
	}
}


/**
 * Heartbeat function
 */
void task_Heartbeat(void)
{
	if(getStopwatch1() > 500)
	{
		static uint8_t heartbeat = false;
		if(heartbeat)
		{
			setLED4(0);
			heartbeat = false;
		}
		else
		{
			setLED4(1);
			heartbeat = true;
		}
		setStopwatch1(0);
	}
}


/*****************************************************************************/
// Main function - The program starts here:

int main(void)
{
	initRobotBase(); 	// Always call this first! The Processor will not work
						// correctly otherwise.		 

	setLEDs(0b1001);		
	mSleep(200); 
   
	I2CTWI_initSlave(I2C_SLAVE_ADR);
	

	writeString_P("\n I2C Slave Control v1\n\n");
	
	if(robot_arm_v3) // This can be used to check for revision of the Robot Arm Board.
	{				 // There are some pinout changes, this is already taken care of in the lib.
					 // But there are also some functional differences, e.g. different current sensors,
					 // more LEDs different expansion options.
		writeString_P("\n ROBOT ARM v3 \n\n");
	}
	
//	Servo_Power_And_Start(); 
// Servos are unpowered by default in this example, you need to enable them using 
// the CMD_POWER_ON command via I2C Bus.

	
	setBeepsound();
	changeBeepsound(100);
	mSleep(50);
	clearBeepsound();
	mSleep(200); 
	
	startStopwatch1();
	
	writeString_P("\n Waiting for commands via I2C Bus!\n\n");
	setLEDs(0b0000);	
	
	while(true)
	{
		if(overcurrent_shutdown)
		{
			writeString_P("\nOVERCURRENT SHUTDOWN!!!\n\n");
			Power_Off_Servos();
			overcurrent_shutdown = 0;
			mSleep(1000);
		}

		if(getStopwatch2() > 400)
		{
			current_check_enable = 1;
			setStopwatch2(0);
			stopStopwatch2();
		}
	
		task_commandProcessor();
		task_updateRegisters();
		task_Heartbeat();
	}
	return 0;
}
