/* 
 * ****************************************************************************
 * Robotarm v3 - ROBOT BASE EXAMPLES
 * ****************************************************************************
 * Example: Robotarm current measurments of servo motors
 * Author(s): Hein wielink
			  Huy Nguyen 
 * ****************************************************************************
 * Description: 
 * In this program you can see the current usage of your robotarm.
 * The actual current will be displayed in terminal, when the current
 * is too high the program disconnect the power of the servos. 
 *
 * This is for Robot Arm v3 and the value displayed in the terminal is
 * converted from raw ADC value to mA.
 * Please note that in the selftest the raw values are displayed!
 * The limits are set in robotarmlib/RobotArmBase.h and are in raw ADC units. 
 *
 * In this Demo, lower values are used to demonstrate the overcurrent limit.
 * Try to move some of the lower servos by hand and see how the current
 * value in the terminal changes. If you reach the limit, it will turn off.
 * (you should of course not push the servos too hard in case it does not work 
 * as intended for some reason e.g. defect ADC channel, mistake in the program 
 * or something like that)
 *
 * If the Arm shuts down even if it is not loaded - this may be due
 * to short current spikes
 *
 * ############################################################################
 * ATTENTION: Make sure you have calibrated the Robotarm or else you
 * can damage de servos!!!
 * You can calibrate the servos with the selftest program. (press c)
 * ############################################################################
 * ****************************************************************************
 */

/*****************************************************************************/
// Includes:

#include "RobotArmBaseLib.h"// The Robotarm Robot Library.
								// Always needs to be included!

/*****************************************************************************/

char receiveBuffer[UART_RECEIVE_BUFFER_SIZE];
int i=1;
	
void highCurrent( int servo, int currentvalue, int max_current ){
	
	writeString_P("\n\nWARNING: SERVO");
	writeIntegerLength(servo, DEC, 1);
	writeString_P(" CURRENT TOO HIGH!\n");
	writeString_P("Read current = ");
	writeIntegerLength(calc_current(currentvalue), DEC, 3);
	writeString_P("\n Max current = ");
	writeIntegerLength(calc_current(max_current), DEC, 3);

	writeString_P("\n\nPress 'x' to restart program\n");
	Power_Off_Servos(); 
	i=0; //Stop program
}	


// Main function - The program starts here:

int main(void)
{
	initRobotBase(); // Always call this first! The Processor will not work
					 // correctly otherwise.	
	
	writeString_P("########################################\n");
	writeString_P("########   Measure Currents  ###########\n\n"); 
	
	mSleep(500);
	writeString_P("\nPower servos:\n");
	
	Servo_Power_And_Start();  //Use this function to power the servos and set them in the center position. 
					 //When you want to power off the servos, you need to call function Power_Off_Servos();  
					  
	mSleep(800);
	writeString_P("\nMeasure currents:\n");
	mSleep(200);
	
	while (true)
	{
		if(i){
		
			// There are several ways to measure the current while using the servos.
			// We show 3 different ones here, only ONE of them should be used
			// at a time, the others are comments only in this code example and do not get compiled. 
			
			// Method 1 to measure current - simply read all ADCs in blocking mode: 
			//Current_1 = readADC(ADC_CURRENT_1); //Read current of servo 1
			//Current_2 = readADC(ADC_CURRENT_2); //Read current of servo 2
			//Current_3 = readADC(ADC_CURRENT_3); //Read current of servo 3
			//Current_4 = readADC(ADC_CURRENT_4); //Read current of servo 4
			//Current_5 = readADC(ADC_CURRENT_5); //Read current of servo 5
			//Current_6 = readADC(ADC_CURRENT_6); //Read current of servo 6
			
			
			// Method 2 to measure current: 
			sampleADCs_and_sleep(250);
	
			/*		
			This function does the following: 
			
			setStopwatch8(0);
			startStopwatch8();
			while(getStopwatch8() < ms)
			{
				task_ADC();
			}
			stopStopwatch8();
			
			So it uses task_ADC to sample all ADC channels in sequence one after another.
			This can be used to measure the current and supply voltage while performing
			other tasks (inside the while loop in this case - here we perform "sleep" only). 
			The ADC values are written to Current_1 to Current_6 variables. 
			*/
	
			// Method 3 to measure current: 
			// sampleADCs_average_and_sleep(250);
			// Works the same as above, but this uses a small averaging filter. 
	
			// This may also be improved further. These are just examples. 
	
			// Output the measured currents: 

			writeString_P("Cur1: ");
			writeIntegerLength(calc_current(Current_1), DEC, 4); //Display current value servo 1
			writeString_P("  |Cur2: ");
			writeIntegerLength(calc_current(Current_2), DEC, 4); //Display current value servo 2
			writeString_P("  |Cur3: ");
			writeIntegerLength(calc_current(Current_3), DEC, 4); //Display current value servo 3
			writeString_P("  |Cur4: ");
			writeIntegerLength(calc_current(Current_4), DEC, 4); //Display current value servo 4
			writeString_P("  |Cur5: ");
			writeIntegerLength(calc_current(Current_5), DEC, 4); //Display current value servo 5
			writeString_P("  |Cur6: ");
			writeIntegerLength(calc_current(Current_6), DEC, 4); //Display current value servo 6	
			writeChar('\n'); 
		
		
			// Now we check for allowed current. 
			
			// Check for maximum allowed current - this is the normal way with thresholds set in
			// the library with RAW ADC values, but for demo purposes we do not use this code: 
		/*	
			if(Current_1 > max_current_servo1_v3){ highCurrent( 1,Current_1,max_current_servo1_v3 );	} //Is the current 1 higher than max current 1?
			if(Current_2 > max_current_servo2_v3){ highCurrent( 2,Current_2,max_current_servo2_v3 );	} //Is the current 2 higher than max current 2?
			if(Current_3 > max_current_servo3_v3){ highCurrent( 3,Current_3,max_current_servo3_v3 );	} //Is the current 3 higher than max current 3?
			if(Current_4 > max_current_servo4_v3){ highCurrent( 4,Current_4,max_current_servo4_v3 );	} //Is the current 4 higher than max current 4?
			if(Current_5 > max_current_servo5_v3){ highCurrent( 5,Current_5,max_current_servo5_v3 );	} //Is the current 5 higher than max current 5?
			if(Current_6 > max_current_servo6_v3){ highCurrent( 6,Current_6,max_current_servo6_v3 );	} //Is the current 6 higher than max current 6?
		*/
	
			// For this demo we limit the current to lower values to show that it will turn off
			// as soon as you push the robot arm to hard. 
			// We convert the raw ADC values to mA with calc_current here!

			if(calc_current(Current_1) > 250){ highCurrent( 1,Current_1,max_current_servo1_v3 );	} //Is the current 1 higher than max current 1?
			if(calc_current(Current_2) > 250){ highCurrent( 2,Current_2,max_current_servo2_v3 );	} //Is the current 2 higher than max current 2?
			if(calc_current(Current_3) > 350){ highCurrent( 3,Current_3,max_current_servo3_v3 );	} //Is the current 3 higher than max current 3?
			if(calc_current(Current_4) > 500){ highCurrent( 4,Current_4,max_current_servo4_v3 );	} //Is the current 4 higher than max current 4?
			if(calc_current(Current_5) > 500){ highCurrent( 5,Current_5,max_current_servo5_v3 );	} //Is the current 5 higher than max current 5?
			if(calc_current(Current_6) > 500){ highCurrent( 6,Current_6,max_current_servo6_v3 );	} //Is the current 6 higher than max current 6?
			// Please note that the DYNAMIC current can be significantly higher
			// than it is in a steady position. This needs to be taken into account when using
			// current limits.
			// You can also see some spikes in current measurement, average current
			// consumption is much lower than the peak values that can be used to detect 
			// overcurrent events. 
			
			
			if(i == 0){ //Wait for 'x' input to restart program
				receiveBytesToBuffer(1, &receiveBuffer[0]);
				if(receiveBuffer[0] == 'x')	{
					writeString_P("########################################\n");
					writeString_P("######  RESTART Measure Currents  ######\n\n");

					Servo_PWM_Zero();
					Power_Servos();
					mSleep(200); 
					Start_position();
					mSleep(800); 
					i=1;
				}
			}
		}	
	}	

	// End of main loop!
	// ---------------------------------------

	return 0;
}
