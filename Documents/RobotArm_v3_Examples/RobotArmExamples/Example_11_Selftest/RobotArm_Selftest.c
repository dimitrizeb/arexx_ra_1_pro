/* 
 * ****************************************************************************
 * Robotarm v3 - EXAMPLES
 * ****************************************************************************
 * Example: Robotarm selftest 
 * Author(s): Hein Wielink
 *	          Huy Nguyen 
 * ****************************************************************************
 * Description:
 *
 * Test routines for all robotarm components. 
 *
 * Yes we know - this program has not the most beautiful code on earth... 
 * To be honest it is quite ugly ;) 
 * All the Text output is just a waste of program space and this is 
 * intentionally to get a large program ;) 
 *
 * It is a good demonstration how big your programs can get! 
 * Consider that every single character of a String consumes one byte 
 * of program space!
 * Also look at it in the hexfile viewer in Robot Loader - nearly half the 
 * program memory is full with ASCII Text. *  
 *
 * #+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+
 * ############################################################################
 * ****************************************************************************
 */
 
/*****************************************************************************/
// Includes:

#include "RobotArmBaseLib.h"

/*****************************************************************************/

char receiveBuffer[UART_RECEIVE_BUFFER_SIZE];

void done(void)
{
	writeString_P("Done!\n"); 
}

void bars(uint8_t number)
{
	uint8_t cnt;
	writeChar('\n');
	for(;number > 0; writeChar('\n'), number--)
		for(cnt = 69; cnt ; cnt--)
			writeChar('#');
}


void test(uint8_t number)
{
	bars(2);
	writeString_P("#### TEST #");
	writeInteger(number, DEC);
	writeString_P(" ####\n");
}


uint16_t update_ubat_adc(void)
{
	//ADMUX = (1<<REFS0) | (0<<REFS1) | (ADC_UBAT<<MUX0);// AVCC
	ADMUX = (1<<REFS0) | (0<<REFS1) | (ADC_UBAT<<MUX0); // INT
	ADCSRA = (0<<ADIE) | (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1);
	mSleep(10);			//waits to become stable after changing the channel
	ADCSRA |= (1<<ADSC); // start Analog to Digital Conversion 
	while ((ADCSRA & (1<<ADSC))); 
	ADCSRA = 0;
	return ADC;
}

void printUBat(uint16_t uBat)
{
	writeIntegerLength((uint16_t)(((((uBat+10) * 2) * 12.56f) / 1024.0f)), DEC, 2);
	writeChar('.');
	writeIntegerLength((uint16_t)(((((uBat+10) * 2) * 12.56f) / 10.24f)), DEC, 2);
	writeChar('V');
}


int overcurrent_events = 0;

void highCurrent( int servo, int currentvalue, int max_current)
{
	
	writeString_P("WARNING: SERVO ");
	writeIntegerLength(servo, DEC, 1);
	writeString_P(" CURRENT TOO HIGH!");
	writeString_P("\n");
	overcurrent_events++;
}

// Robot Arm v3 current measurement with external 5V ref: 
// adc_val = I * 16 * 1024 / 50 
// I = adc_val * 50 / (1024 * 16)
// 3A = 983
// 2A = 655
// 1A = 327
// 0.5A = 163
// 0.1A = 32
// 0.05A = 16
// 0.01A = 3
// If using internal ref, resolution is doubled. 


#define test_max_current_servo1_v3	200
#define test_max_current_servo2_v3	220
#define test_max_current_servo3_v3	350
#define test_max_current_servo4_v3	400
#define test_max_current_servo5_v3	400
#define test_max_current_servo6_v3	400


#define test_min_current_servo1_v3	12
#define test_min_current_servo2_v3	12
#define test_min_current_servo3_v3	12
#define test_min_current_servo4_v3	12
#define test_min_current_servo5_v3	12
#define test_min_current_servo6_v3	12


int c_max1 = 0;
int c_max2 = 0;
int c_max3 = 0;
int c_max4 = 0;
int c_max5 = 0;
int c_max6 = 0;

int c_avg1 = 0;
int c_avg2 = 0;
int c_avg3 = 0;
int c_avg4 = 0;
int c_avg5 = 0;
int c_avg6 = 0;

int servo = 1;



void Display_Current_2(void)	
{
	if(servo == 1) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 1: ");
	writeIntegerLength(Current_1, DEC, 3);

	writeString_P(" |");
	if(servo == 2) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 2: ");
	writeIntegerLength(Current_2, DEC, 3);

	writeString_P(" |");
	if(servo == 3) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 3: ");
	writeIntegerLength(Current_3, DEC, 3);

	writeString_P(" |");
	if(servo == 4) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 4: ");
	writeIntegerLength(Current_4, DEC, 3);

	writeString_P(" |");
	if(servo == 5) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 5: ");
	writeIntegerLength(Current_5, DEC, 3);	

	writeString_P(" |");
	if(servo == 6) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 6: ");
	writeIntegerLength(Current_6, DEC, 3);		

	writeChar('\n'); 

	c_avg1 = (c_avg1 + Current_1) / 2;
	c_avg2 = (c_avg2 + Current_2) / 2;
	c_avg3 = (c_avg3 + Current_3) / 2;
	c_avg4 = (c_avg4 + Current_4) / 2;
	c_avg5 = (c_avg5 + Current_5) / 2;
	c_avg6 = (c_avg6 + Current_6) / 2;
	
	if(Current_1 > c_max1) c_max1 = Current_1;
	if(Current_2 > c_max2) c_max2 = Current_2;
	if(Current_3 > c_max3) c_max3 = Current_3;
	if(Current_4 > c_max4) c_max4 = Current_4;
	if(Current_5 > c_max5) c_max5 = Current_5;
	if(Current_6 > c_max6) c_max6 = Current_6;
	
	if(Current_1 > test_max_current_servo1_v3){ highCurrent( 1,Current_1,test_max_current_servo1_v3 );	}
	if(Current_2 > test_max_current_servo2_v3){ highCurrent( 2,Current_2,test_max_current_servo2_v3 );	}
	if(Current_3 > test_max_current_servo3_v3){ highCurrent( 3,Current_3,test_max_current_servo3_v3 );	}
	if(Current_4 > test_max_current_servo4_v3){ highCurrent( 4,Current_4,test_max_current_servo4_v3 );	}
	if(Current_5 > test_max_current_servo5_v3){ highCurrent( 5,Current_5,test_max_current_servo5_v3 );	}
	if(Current_6 > test_max_current_servo6_v3){ highCurrent( 6,Current_6,test_max_current_servo6_v3 );	}
}





void Display_Current(void)	
{
	Current_1 = readADC(ADC_CURRENT_1); 
	if(servo == 1) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 1: ");
	writeIntegerLength(Current_1, DEC, 3);

	Current_2 = readADC(ADC_CURRENT_2); 
	writeString_P(" |");
	if(servo == 2) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 2: ");
	writeIntegerLength(Current_2, DEC, 3);

	Current_3=readADC(ADC_CURRENT_3); 
	writeString_P(" |");
	if(servo == 3) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 3: ");
	writeIntegerLength(Current_3, DEC, 3);

	Current_4=readADC(ADC_CURRENT_4); 
	writeString_P(" |");
	if(servo == 4) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 4: ");
	writeIntegerLength(Current_4, DEC, 3);

	Current_5=readADC(ADC_CURRENT_5); 
	writeString_P(" |");
	if(servo == 5) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 5: ");
	writeIntegerLength(Current_5, DEC, 3);	

	Current_6=readADC(ADC_CURRENT_6); 
	writeString_P(" |");
	if(servo == 6) writeChar('#'); else writeChar(' ');
	writeString_P("Cur 6: ");
	writeIntegerLength(Current_6, DEC, 3);		

	writeChar('\n'); 

	c_avg1 = (c_avg1 + Current_1) / 2;
	c_avg2 = (c_avg2 + Current_2) / 2;
	c_avg3 = (c_avg3 + Current_3) / 2;
	c_avg4 = (c_avg4 + Current_4) / 2;
	c_avg5 = (c_avg5 + Current_5) / 2;
	c_avg6 = (c_avg6 + Current_6) / 2;
	
	if(Current_1 > c_max1) c_max1 = Current_1;
	if(Current_2 > c_max2) c_max2 = Current_2;
	if(Current_3 > c_max3) c_max3 = Current_3;
	if(Current_4 > c_max4) c_max4 = Current_4;
	if(Current_5 > c_max5) c_max5 = Current_5;
	if(Current_6 > c_max6) c_max6 = Current_6;
	
	if(Current_1 > test_max_current_servo1_v3){ highCurrent( 1,Current_1,test_max_current_servo1_v3 );	}
	if(Current_2 > test_max_current_servo2_v3){ highCurrent( 2,Current_2,test_max_current_servo2_v3 );	}
	if(Current_3 > test_max_current_servo3_v3){ highCurrent( 3,Current_3,test_max_current_servo3_v3 );	}
	if(Current_4 > test_max_current_servo4_v3){ highCurrent( 4,Current_4,test_max_current_servo4_v3 );	}
	if(Current_5 > test_max_current_servo5_v3){ highCurrent( 5,Current_5,test_max_current_servo5_v3 );	}
	if(Current_6 > test_max_current_servo6_v3){ highCurrent( 6,Current_6,test_max_current_servo6_v3 );	}
}

/*****************************************************************************/

void StatusLedTest(void)
{	
	test(1);
	writeString_P("\n### LED TEST ###\n");
	writeString_P("Please watch the blue LEDs and verify that they all light up!\n");
	writeString_P("\n");
	setLEDs(0b0001);
	mSleep(500);
	setLEDs(0b0011);
	mSleep(500);
	setLEDs(0b0111);
	mSleep(500);
	setLEDs(0b1111);
	mSleep(500);
	setLEDs(0b1110);
	mSleep(500);
	setLEDs(0b1100);
	mSleep(500);
	setLEDs(0b1000);
	mSleep(500);
	setLEDs(0b0000);
	mSleep(500);
	done();
}


/*****************************************************************************/
//


void VoltageTest(void)
{
	test(3);
	writeString_P("\n### Voltage Sensor Test ###\n");
	writeString_P("\n Press ENTER to start battery measurements!\n\n");
    receiveBytesToBuffer(1, &receiveBuffer[0]);

	writeString_P("Performing 10 measurements:\n");
	uint16_t ubat;
	uint8_t i;
	for(i = 1; i <= 10; i++)
	{
		writeString_P("Measurement #"); 
		writeInteger(i, DEC);
		writeString_P(": ");
		ubat = update_ubat_adc(); //;readADC(ADC_UBAT);
		printUBat(ubat);

		if(ubat >= 275 && ubat <= 650)
		{
			writeString_P(" --> OK!\n");	
		}
		else
		{
			if(ubat < 275) 
			{
				writeString_P("WARNING: VOLTAGE IS TOO LOW!\n");
			}
			else if(ubat > 650)
			{
				writeString_P("WARNING: Voltage is quite high!\n!");
			}
		}
		
		mSleep(50);
	
	}
	mSleep(500); 
	done();
}



/*****************************************************************************/

void BuzzerTest(void)
{	
	test(2);
	writeString_P("Buzzer Test\n");
	writeString_P("Please listen to the Buzzer and verify that it works!\n");
	writeString_P("The Buzzer will beep a few times!\n");
	writeString_P("### The test is running now! ### \n\n");

	setLEDs(0b1000);

	clearBeepsound();
	mSleep(200); 
	setBeepsound();
	changeBeepsound(50);
	mSleep(50);
	clearBeepsound();
	mSleep(200); 
	
	setLEDs(0b0001);
	
	setBeepsound();
	changeBeepsound(80);
	mSleep(50);
	clearBeepsound();
	mSleep(200); 
   
   	setLEDs(0b1000);
   
	setBeepsound();
	changeBeepsound(120);
	mSleep(50);
	clearBeepsound();
	mSleep(200); 
   
	setLEDs(0b0001);
   
	setBeepsound();
	changeBeepsound(150);
	mSleep(50);
	clearBeepsound();
	mSleep(200); 
	
	setLEDs(0b1000);
	
	setBeepsound();
	changeBeepsound(180);
	mSleep(50);
	clearBeepsound();
	mSleep(100); 
   
	setLEDs(0b0001);
   
	setBeepsound();
	changeBeepsound(220);
	mSleep(100);
	clearBeepsound();
	mSleep(100); 
   
    setLEDs(0b1000);
   
	setBeepsound();
	changeBeepsound(255);
	mSleep(100);
	clearBeepsound();

	setLEDs(0b0000);

	done();
}

int noloadtoohigh_event = 0;

void no_load_current_too_high(uint8_t servo)
{
	writeString_P("### WARNING: Too high current at servo #");
	writeIntegerLength(servo, DEC, 1);
	writeString_P(" in no load state ### \n");
	noloadtoohigh_event++;
}

void ServoPowerTest(void)
{
	test(4);
	writeString_P("Servo Power Test\n");
	writeString_P("Please observe the RED Led SERVO_POWER!\n");
	writeString_P("Check if it turns on and off!\n");
	writeString_P("This test also checks zero load current measurement values.\n");
	writeString_P("### The test is running now! ### \n\n");

	Pos_Servo_1=0;
	Pos_Servo_2=0;
	Pos_Servo_3=0;
	Pos_Servo_4=0;
	Pos_Servo_5=0;
	Pos_Servo_6=0;
	
	setBeepsound();
	changeBeepsound(125);
	mSleep(100);
	clearBeepsound();
	mSleep(100); 

	Power_Servos();
	mSleep(250); 
	noloadtoohigh_event = 0;
	
	for(uint8_t i = 0; i < 9; i++)
	{
		Display_Current();
		if(Current_1 > 20) no_load_current_too_high(1);
		if(Current_2 > 20) no_load_current_too_high(2);
		if(Current_3 > 20) no_load_current_too_high(3);
		if(Current_4 > 20) no_load_current_too_high(4);
		if(Current_5 > 20) no_load_current_too_high(5);
		if(Current_6 > 20) no_load_current_too_high(6);
		mSleep(250);
	}
	
	Power_Off_Servos();
	
	if(noloadtoohigh_event > 3)
	{
		writeString_P("\n########################################\n");
		writeString_P("WARNING: NO LOAD CURRENT TOO HIGH!\n");
		writeString_P("Check current sensors and power supply!\n");
		writeString_P("########################################\n");
	}
	
	setBeepsound();
	changeBeepsound(250);
	mSleep(100);
	clearBeepsound();
	
	done();
}


void s_Move_test (uint8_t Servo, int16_t D_Value, uint16_t Speed) 
{
	int16_t Actual_position=2700;  
	
	switch (Servo) 
	{
		case 1:	Actual_position = Pos_Servo_1 - Start_Position[1]; break; 
		case 2:	Actual_position = Pos_Servo_2 - Start_Position[2]; break; 
		case 3:	Actual_position = Pos_Servo_3 - Start_Position[3]; break; 
		case 4:	Actual_position = Pos_Servo_4 - Start_Position[4]; break; 
		case 5:	Actual_position = Pos_Servo_5 - Start_Position[5]; break; 
		case 6:	Actual_position = Pos_Servo_6 - Start_Position[6]; break; 
	}
	
	
	if (Actual_position > D_Value )
	{ 
		while (Actual_position > D_Value)
		{
			
			task_ADC();
			Actual_position--; 
			Move(Servo, Actual_position);
			//mSleep(Speed);
			
			// Instead of mSleep, perform ADC readings while waiting:
			sampleADCs_and_sleep(Speed);
			
			if(Actual_position % 30 == 0)
				Display_Current_2();
		}
		return; 
	}
	
	if (Actual_position < D_Value )
	{ 
		while (Actual_position < D_Value)
		{
			Actual_position++; 
			Move(Servo, Actual_position);
			//mSleep(Speed);
			
			// Instead of mSleep, perform ADC readings while waiting:
			
			sampleADCs_and_sleep(Speed);
			
			stopStopwatch8();
			if(Actual_position % 30 == 0)
				Display_Current_2();
		}
		return;
	}	
}

void print_max_current_values(void)
{
	writeString_P("\n\n##########################################");
	writeString_P("\nMax current measurement values (PEAK!):");
	writeString_P("\n##########################################");
	writeString_P("\n SERVO1 max: ");
	writeInteger(c_max1, DEC);
	writeString_P("\t (");
	writeInteger(calc_current(c_max1), DEC); 
	writeString_P("mA)\n SERVO2 max: ");
	writeInteger(c_max2, DEC);
	writeString_P("\t (");
	writeInteger(calc_current(c_max2), DEC); 
	writeString_P("mA)\n SERVO3 max: ");
	writeInteger(c_max3, DEC);
	writeString_P("\t (");
	writeInteger(calc_current(c_max3), DEC); 
	writeString_P("mA)\n SERVO4 max: ");
	writeInteger(c_max2, DEC);
	writeString_P("\t (");
	writeInteger(calc_current(c_max4), DEC); 
	writeString_P("mA)\n SERVO5 max: ");
	writeInteger(c_max5, DEC);
	writeString_P("\t (");
	writeInteger(calc_current(c_max5), DEC); 
	writeString_P("mA)\n SERVO2 max: ");
	writeInteger(c_max6, DEC);
	writeString_P("\t (");
	writeInteger(calc_current(c_max6), DEC); 
	writeString_P("mA)\n");

	writeString_P("###########################################\n\n");
}

void ServoTest(void)
{
	test(5);
	writeString_P("\nAutomatic Servomotor test\n\n");
	writeString_P("###############################################\n");
	writeString_P("### ATTENTION!!! DANGER!!! WARNING!!!\n");
	writeString_P("Make sure the servomotors can move FREE! DO NOT BLOCK THEM!\n");
	writeString_P("###############################################\n");
	writeString_P("\n Press ENTER to start servo test!\n");
    receiveBytesToBuffer(1, &receiveBuffer[0]);

	c_max1 = 0;
	c_max2 = 0;
	c_max3 = 0;
	c_max4 = 0;
	c_max5 = 0;
	c_max6 = 0;

	Servo_Power_And_Start();

	//Start_position();
	//Power_Servos();
	
	writeString_P("Set Start Position: \n");
	uint16_t pos;
	
	mSleep(500);
	servo = 1;
	overcurrent_events=0;
	
	writeString_P("\nServo: 1 moves\n");
	
	s_Move_test(1, -400,2);	
	s_Move_test(1, 0,2);	
	mSleep(200);

	for (servo = 2; servo < 7 ; servo++)
	{ 
		writeString_P("\n###### Servo: ");
		writeInteger(servo,DEC);
		writeString_P(" moves #######\n");
		pos = Start_Position[servo];

		s_Move_test(servo, 500,2);
		s_Move_test(servo, -500,2);	
		s_Move_test(servo, 0,2);	
	}
	
	Power_Off_Servos();
	
	print_max_current_values();
	
	int c_error = 0;

	if(c_max1 < test_min_current_servo1_v3)
	{
		writeString_P("\n### SERVO 1 minimal measured current was too low! Check current sensor circuit!\n");
		c_error++;
	}
	if(c_max2 < test_min_current_servo2_v3)
	{
		writeString_P("\n### SERVO 2 minimal measured current was too low! Check current sensor circuit!\n");
		c_error++;
	}
	if(c_max3 < test_min_current_servo3_v3)
	{
		writeString_P("\n### SERVO 3 minimal measured current was too low! Check current sensor circuit!\n");
		c_error++;
	}
	if(c_max4 < test_min_current_servo4_v3)
	{
		writeString_P("\n### SERVO 4 minimal measured current was too low! Check current sensor circuit!\n");
		c_error++;
	}
	if(c_max5 < test_min_current_servo5_v3)
	{
		writeString_P("\n### SERVO 5 minimal measured current was too low! Check current sensor circuit!\n");
		c_error++;
	}
	if(c_max6 < test_min_current_servo6_v3)
	{
		writeString_P("\n### SERVO 6 minimal measured current was too low! Check current sensor circuit!\n");
		c_error++;
	}
	
	if(overcurrent_events > 2) {
		if(c_max1 > test_max_current_servo1_v3)
		{
			writeString_P("\n### SERVO 1 maximal measured current was too high! Check current sensor circuit!\n");
			c_error++;
		}
		if(c_max2 > test_max_current_servo2_v3)
		{
			writeString_P("\n### SERVO 2 maximal measured current was too high! Check current sensor circuit!\n");
			c_error++;
		}
		if(c_max3 > test_max_current_servo3_v3)
		{
			writeString_P("\n### SERVO 3 maximal measured current was too high! Check current sensor circuit!\n");
			c_error++;
		}
		if(c_max4 > test_max_current_servo4_v3)
		{
			writeString_P("\n### SERVO 4 maximal measured current was too high! Check current sensor circuit!\n");
			c_error++;
		}
		if(c_max5 > test_max_current_servo5_v3)
		{
			writeString_P("\n### SERVO 5 maximal measured current was too high! Check current sensor circuit!\n");
			c_error++;
		}
		if(c_max6 > test_max_current_servo6_v3)
		{
			writeString_P("\n### SERVO 6 maximal measured current was too high! Check current sensor circuit!\n");
			c_error++;
		}
		if(c_error > 0) 
		{
			writeString_P("\n\n##############################");
			writeString_P("\nWarning: Test finished with ERRORS!");
			writeString_P("\n##############################\n\n");
		}
	}
	
	done();
}


/*****************************************************************************/
// 

void calibration(void)
{
	Servo_Power_And_Start_Default(); // Set servos to default start pos and power on - with not properly set values in EEPROM.

	startStopwatch1();
	//defines:
	char dir = 0;	
	int CalServo = 1;
	signed int position = 0;

	// ---------------------------------------
	// Write messages to the Serial Interface
	writeString_P("#### Calibration #####\n");
	writeString_P("To calibrate the robot arm in the right position, you have to calibrate \n");
	writeString_P("the robot arm in the same position as the picture in the manual\n");
	writeString_P("(chapter: calibration)\n\n");	
	writeString_P("You can use the next keys to change the position:\n");
	writeString_P("Enter h:  Stop the servo movement (hold)\n");
	writeString_P("Enter p:  Servo position +  \n");
	writeString_P("Enter m:  Servo position - \n");
	writeString_P("Enter ok: If the servo stands in the correct position,\n");
	writeString_P("Enter x:  It will stop de calibration program\n");
	writeString_P(" \n\n");

	receiveBytes(2);
	writeString_P("#### Calibrate Servo 1 #####\n");
	writeString_P("Please enter your choice: h,p,m,ok,x\n");

	Start_Position[1] = 1500; 	
	

	while(CalServo<7)
	{
		if(getUARTReceiveStatus() != UART_BUISY)
		{
			copyReceivedBytesToBuffer(&receiveBuffer[0]);
				
			
				
			if(receiveBuffer[0]=='o' && receiveBuffer[1]=='k' )
			{
				Start_Position[CalServo] = position + 1500;
				
				writeString_P("Servo ");
				writeInteger(CalServo,DEC);
				writeString_P(" default value is: ");
				writeInteger(Start_Position[CalServo],DEC);
				writeString_P("\n\n");
				CalServo ++;
				
				if(CalServo<7)
				{
					Start_Position[CalServo] = 1500; 	
					position = 0;
					dir=0;
					writeString_P("#### Calibrate Servo ");
					writeInteger(CalServo, DEC);
					writeString_P(" ####\n");
					writeString_P("Please enter your choice: h,p,m,ok or x\n");
			
				}
			}

			else if(receiveBuffer[0]=='x')
			{
			    writeString_P("END\n");
				break;				
			}

			else if(receiveBuffer[0]=='h')
			{
				dir = 0;
			    writeString_P("Stop (hold) \n");
			}

			else if (receiveBuffer[0]=='p')
			{
			    writeString_P("Plus\n");
				dir = 1;
			}

			else if (receiveBuffer[0]=='m')
			{
			    writeString_P("Minus\n");
				dir = 2; 
			}

			else 
			{
			writeString_P("incorrect command\n");
			}

			receiveBytes(2);
		}
		
		if (dir==1)
			{
			position+=1;
			}
		
		else if(dir==2)  
			{
			position-=1;
			}
				
			Move(CalServo, position);
			mSleep(15);
			
		if(getStopwatch1() > 1000 && dir!= 0) // have we reached AT LEAST 1000ms = 1s?
				{
				writeString_P("Position: ");
				writeInteger( ( position + 1500 ),DEC);
				writeString_P("\n");
				setStopwatch1(0);  
				}
			
	}


	
	for (int j=1;j<CalServo;j++)
	{
			
	writeString_P("Default value for servo ");
	writeInteger(j,DEC);
	writeString_P(" = ");
	writeInteger(Start_Position[j],DEC);
	writeString_P("\n");
	}

	writeString_P("\nWrite values to EEprom   ...   ");
	
	write_Values_EE();		// To write EEPROM from SRAM
	
	writeString_P("Success!\n");
	
	Power_Off_Servos();
	stopStopwatch1();
	
	writeString_P("Press ENTER to return to main screen\n");
	
	receiveBytesToBuffer(1, &receiveBuffer[0]);
}

//Current measuring:

void current_measuring (void)
{
	bars(2);
	writeString_P("#### Current Measuring #####\n");

	writeString_P("Press ENTER to return to main screen!\n\n");
	writeString_P("### The test is running now! ### \n\n");
	
	receiveBytes(1);
	Servo_Power_And_Start();
	//Start_position();
	//Power_Servos();
	mSleep(150);

	task_ADC();
	
	while(true)
	{
		if(getUARTReceiveStatus() != UART_BUISY)
		{	
			copyReceivedBytesToBuffer(&receiveBuffer[0]);
			break;
		}
		 
		Display_Current_2(); 
		//mSleep(200);
		// Instead of mSleep, perform ADC readings while waiting:
		sampleADCs_and_sleep(100);
	}

	print_max_current_values();

	Power_Off_Servos();	
	
	writeString_P(" #### End #####\n\n");
}

//Start position 

void Set_Start_position (void)
{
	bars(2);
	writeString_P("#### Startposition Robotarm#####\n");

	Servo_Power_And_Start();
	//Start_position();
    //Power_Servos();

	mSleep(3000);
	
	Power_Off_Servos();
	done();
}


void keyboard_test(void)
{
	bars(2);
	writeString_P("#### Keyboard Quick Test #####\n");
	writeString_P("### The test is running now! ### \n\n");

	receiveBytes(1);
	
	int state = 0;
	
	while(true)
	{
		if(getUARTReceiveStatus() != UART_BUISY)
		{	
			copyReceivedBytesToBuffer(&receiveBuffer[0]);
			break;
		}
		int key = scan_keyboard();
		if(key)
		{
			setLEDs(0b1000);
			writeString_P("Key: ");
			writeInteger(key, DEC);
			writeString_P("\n");
					
			switch(key)
			{
				case 0: { break;}
				case 1: { writeString_P("S1-\n"); break;}
				case 2: { writeString_P("S1+\n"); break;}
				case 3: { writeString_P("S2-\n"); break;}
				case 4: { writeString_P("S2+\n"); break;}
				case 5: { writeString_P("S3-\n"); break;}
				case 6: { writeString_P("S3+\n"); break;}
				case 7: { writeString_P("S4-\n"); break;}
				case 8: { writeString_P("S4+\n"); break;}
				case 9: { writeString_P("S5-\n"); break;}
				case 10: { writeString_P("S5+\n"); break;}
				case 11: { writeString_P("S6-\n"); break;}
				case 12: { writeString_P("S6+\n"); break;}
				case 13: { writeString_P("TANK FNT\n"); break;}
				case 14: { writeString_P("TANK BCK\n"); break;}
				case 15: { writeString_P("TANK RIGHT\n"); break;}
				case 16: { writeString_P("TANK LEFT\n"); break;}
			}
			mSleep(150);
		}
		
		switch(state)
		{
			case 0: 
				writeString_P("\nPress key \"SERVO1 UP\" (Key 1)!\n"); 
				state = 1;
				break;
			case 1: 
				if(key == 1)
				{
					writeString_P("### OK!\n");
					writeString_P("\nPress key \"SERVO3 DWN\" (Key 6)!\n"); 
					state = 2;
					mSleep(50);
				}
				else if(key != 0)
				{
					setLEDs(0b1111);
					writeString_P("#################################\n");
					writeString_P("#### ERROR WRONG KEY DETECTED!!!\n");
					writeString_P("#################################\n");
					mSleep(200);
				}
				break;
			case 2:
				if(key == 6)
				{
					writeString_P("### OK!\n");
					writeString_P("\nPress key \"SERVO6 UP\" (Key 11)!\n"); 
					state = 3;
					mSleep(50);
				}
				else if(key != 0)
				{
					setLEDs(0b1111);
					writeString_P("#################################\n");
					writeString_P("#### ERROR WRONG KEY DETECTED!!!\n");
					writeString_P("#################################\n");
					mSleep(200);
				}
				break; 
			case 3:
				if(key == 11)
				{
					writeString_P("### OK!\n");
					writeString_P("\nPress key \"TANK LEFT\" (Key 16)!\n"); 
					state = 4;
					mSleep(50);
				}
				else if(key != 0)
				{
					setLEDs(0b1111);
					writeString_P("#################################\n");
					writeString_P("#### ERROR WRONG KEY DETECTED!!!\n");
					writeString_P("#################################\n");
					mSleep(200);
				}
				break; 
			case 4:
				if(key == 16)
				{
					writeString_P("### OK!\n");
					writeString_P("\n*** TEST FINISHED OK! KEYBOARD IS WORKING! ***\n"); 
					state = 5;
					mSleep(50);
				}
				else if(key != 0)
				{
					setLEDs(0b1111);
					writeString_P("#################################\n");
					writeString_P("#### ERROR WRONG KEY DETECTED!!!\n");
					writeString_P("#################################\n");
					mSleep(200);
				}
				break; 
			case 5:
				break;
		}
		
		while(key)
		{
			key = scan_keyboard();
			setLEDs(0b0001);
			mSleep(2);
		}
		
		if(state == 5)
		{
			break;
		}
		
		mSleep(2);
	}
	
	writeString_P(" #### End #####\n\n");
}


void keyboard_move(void)
{
	bars(2);
	writeString_P("#### Move Servos with the Keyboard!#####\n");

	writeString_P("Press ENTER to return to main screen!\n\n");
	writeString_P("### The test is running now! ### \n\n");
	
	receiveBytes(1);
	Servo_Power_And_Start();
	mSleep(250);
	
	uint8_t speed = 1;
	uint8_t delay_output = 0;
	
	while(true)
	{
		if(getUARTReceiveStatus() != UART_BUISY)
		{	
			copyReceivedBytesToBuffer(&receiveBuffer[0]);
			break;
		}
		
		uint8_t keyb = scan_keyboard();
		switch(keyb){
			case 0: { // Nothing happend!
				setLEDs(0b0000); 
				sampleADCs_and_sleep(100);
				Display_Current_2(); 
				break;
			} 
			case 1: { Pos_Servo_1--; break;}
			case 2: { Pos_Servo_1++; break;}
			case 3: { Pos_Servo_2--; break;}
			case 4: { Pos_Servo_2++; break;}
			case 5: { Pos_Servo_3--; break;}
			case 6: { Pos_Servo_3++; break;}
			case 7: { Pos_Servo_4--; break;}
			case 8: { Pos_Servo_4++; break;}
			case 9: { Pos_Servo_5--; break;}
			case 10: {  Pos_Servo_5++; break;}
			case 11: {  Pos_Servo_6--; break;}
			case 12: {  Pos_Servo_6++; break;}
			case 13: 
				writeString_P("\n SPEED INCREASE +++"); 
				speed--; 
				if(speed < 2) speed = 1;
				setLEDs(0b0001); 
				mSleep(500);
				break;
			case 14: 
				writeString_P("\n SPEED DECREASE ---"); 
				speed++; 
				if(speed > 15) speed = 15;
				setLEDs(0b0010); 
				mSleep(500);
				break;
			case 15: 
				writeString_P("\n SPEED INCREASE +++"); 
				speed--; 
				if(speed < 2) speed = 1;
				setLEDs(0b0100); 
				mSleep(500);
				break;
			case 16: 
				writeString_P("\n SPEED DECREASE ---"); 
				speed++; 
				if(speed > 15) speed = 15;
				setLEDs(0b1000); 
				mSleep(500);
				break;
		}
		
		sampleADCs_and_sleep(speed);
		if(keyb)
		{	
			delay_output++;
			if(delay_output > 25)
			{
				writeString_P(" KEY:"); 
				writeInteger(keyb, 10);
				writeString_P("| ");
				Display_Current_2(); 
				delay_output = 0;
			}
		}
		
	}

	print_max_current_values();

	Power_Off_Servos();	
	
	writeString_P(" #### End #####\n\n");
}


/*****************************************************************************/
// Main:


int main (void)
{
	uint8_t test = 0;
	initRobotBase();

	if(!robot_arm_v3)
	{
		writeString_P("\n ERROR: This Test is intended for Robot ARM v3 PCB!\n");
		writeString_P("\n For older revisions you need the old test program!\n");
		while(true){}
	}
	
	Power_Off_Servos();
	
	writeString_P("#####################################################################\n");
	writeString_P("v3 v3 v3 v3 v3 v3 v3\n");
	writeString_P("\nRobotarm v3 Selftest\n");
	writeString_P("\nv3 v3 v3 v3 v3 v3 v3\n");
	writeString_P("#####################################################################\n");
	writeString_P("### ATTENTION!!! DANGER!!! WARNING!!!\n");
	writeString_P("\n");
	writeString_P("If this is the first use of this robot arm: \n");
	writeString_P("1) Type 'c' in the following main menu to calibrate the robot arm\n");
	writeString_P("2) Calibrate the robotarm servomotors\n");
	writeString_P("\n");
	writeString_P("#####################################################################\n\n");
	writeString_P("Press ENTER to continue!\n"); 

	receiveBytesToBuffer(1, &receiveBuffer[0]);
	
	while(1)
	{
		// This menu is mainly a program space filler - good for serial interface (speed) testing ;)
		// The same applies for ALL other text information in this program!
		// All this is just to have a BIG program download ;)
		writeChar('\n');
		writeString_P("#####################################################################\n"); 
		writeString_P("#########               Robotarm v3 Selftest                #########\n"); 
	    writeString_P("#####################################################################\n"); 
		writeString_P("#####       Main Menu         #########      Advanced Menu      #####\n"); 
		writeString_P("#                                 #                                 #\n"); 
		writeString_P("# 0 - Run ALL Selftests (1-5)     # c - Calibrate Robotarm          #\n"); 
		writeString_P("# 1 - LED Test                    # m - Current Measuring Servos    #\n"); 
		writeString_P("# 2 - Buzzer Test                 # b - Start Position              #\n"); 
		writeString_P("# 3 - Voltage Sensor Test         # k - Keyboard Test               #\n"); 
		writeString_P("# 4 - Servo Power Test            # s - Move Servos with Keyboard   #\n"); 
		writeString_P("# 5 - Servo Test                  #                                 #\n"); 
		writeString_P("#                                 # System voltage is: ");
		printUBat(update_ubat_adc());						 
		writeString_P("       #\n"); 							
		writeString_P("#                                 #                                 #\n"); 
		writeString_P("#####################################################################\n"); 
		writeString_P("# Please enter your choice (0-5, c, m, b, k or s)!                  #\n");
		writeString_P("#####################################################################\n"); 
		
		receiveBytesToBuffer(1, &receiveBuffer[0]);
		
		test = receiveBuffer[0] - 48;
		
		if(receiveBuffer[0] == 'c')
		{
			// Calibrate the Robotarm
			calibration();
		}
		else if(receiveBuffer[0] == 'm')
		{
			// Current Measuring 
			current_measuring();
		}
		else if(receiveBuffer[0] == 'b')
		{
			// Robotarm in startposition
			Set_Start_position();
		}
		else if(receiveBuffer[0] == 'k')
		{
			// Robotarm in startposition
			keyboard_test();
		}
		else if(receiveBuffer[0] == 's')
		{
			// Robotarm in startposition
			keyboard_move();
		}
		else if(test > 5)
		{
			writeString_P("You need to enter a single number from 0 to 5, c, m or b!");
			continue;
		}
		else
		{
			switch(test)
			{
				case 0: StatusLedTest();	
						BuzzerTest();
						VoltageTest();
						ServoPowerTest();
						ServoTest();
				break; 	
				case 1: StatusLedTest();	break; 
				case 2: BuzzerTest();		break;
				case 3: VoltageTest();		break; 	
				case 4: ServoPowerTest();	break; 
				case 5: ServoTest();		break;
			
			}
		}

	}
	
	return 0;
}
